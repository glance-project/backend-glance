require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const { connect } = require('mongoose');
const router = require('./src/router');
const PORT = process.env.PORT || 4000;
const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use('/api', router);

const start = async () => {
  try {
    await connect(process.env.DB_URL);
    app.listen(PORT, () => console.warn(`App listening on port ${PORT}`));
  } catch (error) {
    console.log('Error server: ', error);
  }
};

start().catch((error) => console.warn('Ошибка при подключение к серверу: ', error));

