const { Schema, model } = require('mongoose');

const FiltersModel = new Schema(
  {
    _id: { type: String, required: true },
    filters: {
      price: {
        min: String,
        max: String,
      },
      memories: Array,
      ram: Array,
      brands: Array,
    },
  },
  {
    versionKey: null
  }
);

module.exports = model('Filters', FiltersModel);
