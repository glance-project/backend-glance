const { Schema, model } = require('mongoose');

const PostModel = new Schema(
  {
    name: { type: String, required: true },
    title: { type: String },
  },
  {
    versionKey: null
  }
);

module.exports = model('PostModel', PostModel);
