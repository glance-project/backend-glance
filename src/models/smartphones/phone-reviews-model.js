const { Schema } = require('mongoose');

const PhoneReviewsSchema = new Schema(
  {
    _id: String,
    grade: Number,
    url: String,
    name: String,
    plus: String,
    minus: String,
    comment: String,
  }
);

module.exports = PhoneReviewsSchema;
