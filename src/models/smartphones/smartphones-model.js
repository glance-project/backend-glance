const { Schema, model } = require('mongoose');

const SmartphonesSchema = new Schema(
  {
    _id: String,
    brand: String,
    model: String,
    memory: String,
    colors: Array,
    isNew: Boolean,
    images: Array,
    price: String,
    actualPrice: String
  }
);

module.exports = model('Smartphones', SmartphonesSchema);
