const { Schema, model } = require('mongoose');

const SmartphoneCharacteristicsSchema = new Schema(
  {
    _id: String,
    characteristics: Array,
  }
);

module.exports = model('smartphone-characteristics', SmartphoneCharacteristicsSchema);
