const { Schema, model } = require('mongoose');

const ProductsSchema = new Schema(
  { _id: String, brand: String, products: Array},
  { collection : 'brands' }
);

module.exports = model('Products', ProductsSchema);
