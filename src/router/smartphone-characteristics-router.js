const Router = require('express');
const router = new Router();
const SmartphoneCharacteristicsController = require('../controllers/smartphones/smartphone-characteristics-controller');

router.get('/smartphone-characteristics', SmartphoneCharacteristicsController.getSmartphoneCharacteristics);

module.exports = router;
