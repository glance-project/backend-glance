const Router = require('express');
const router = new Router();
const FiltersController = require('../controllers/filters-controller');

router.get('/filters-smartphones', FiltersController.getFilters);

module.exports = router;
