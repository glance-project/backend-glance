const Router = require('express');
const router = new Router();
const PostController = require('../controllers/post-controller');

router.post('/post', PostController.setPost);
router.get('/posts', PostController.getPosts);
router.get('/post/:id', PostController.getPost);
router.put('/post', PostController.editPost);
router.delete('/post/:id', PostController.removePost);
router.delete('/posts', PostController.removeAllPosts);

module.exports = router;

