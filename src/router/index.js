// const Posts = require('./post-router');
// const Products = require('./products-router');
const Smartphones = require('./smartphones-router');
const Reviews = require('./phone-reviews-router');
const SmartphoneCharacteristics = require('./smartphone-characteristics-router');
const Filters = require('./filters-router');

module.exports = [
  // Posts,
  // Products,
  Smartphones,
  Reviews,
  SmartphoneCharacteristics,
  Filters,
];
