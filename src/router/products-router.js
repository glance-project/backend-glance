const Router = require('express');
const router = new Router();
const ProductsController = require('../controllers/products-controller');

router.get('/products', ProductsController.getProducts);

module.exports = router;
