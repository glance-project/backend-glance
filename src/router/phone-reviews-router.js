const Router = require('express');
const router = new Router();
const PhoneReviewsController = require('../controllers/smartphones/phone-reviews-controller');

router.get('/phone-reviews', PhoneReviewsController.getReviews);

module.exports = router;
