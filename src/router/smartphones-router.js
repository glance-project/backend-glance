const Router = require('express');
const router = new Router();
const SmartphonesController = require('../controllers/smartphones/smartphones-controller');

router.get('/smartphones', SmartphonesController.getSmartphones);
router.get('/smartphone', SmartphonesController.getSmartphone);

module.exports = router;
