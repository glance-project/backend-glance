const FiltersService = require('../services/filters-service');

class FiltersController {
  async getFilters(req, res) {
    try {
      const filters = await FiltersService.getFilters();
      res.status(200).json(filters);
    } catch (error) {
      res.status(400).json({ error })
    }
  }
}

module.exports = new FiltersController();
