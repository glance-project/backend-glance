const PhoneReviewsService = require('../../services/smartphones/phone-reviews-service');

class PhoneReviewsController {
  async getReviews(req, res) {
    try {
      const reviews = await PhoneReviewsService.getReviews(req.query?.id);
      res.status(200).json(reviews);
    } catch {
      res.status(400).json({error: 'Не указан Id выбранного телефона'})
    }
  }
}

module.exports = new PhoneReviewsController();
