const SmartphonesService = require('../../services/smartphones/smartphones-service');

class SmartphonesController {
  async getSmartphones(req, res) {
    try {
      const smartphones = await SmartphonesService.getSmartphones(req.query);
      res.status(200).json(smartphones);
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  async getSmartphone(req, res) {
    try {
      const smartphone = await SmartphonesService.getSmartphone(req.query?._id);
      res.status(200).json(smartphone);
    } catch (error) {
      res.status(400).json({ error })
    }
  }
}

module.exports = new SmartphonesController();
