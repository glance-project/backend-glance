const SmartphoneCharacteristicsService = require('../../services/smartphones/smartphone-characteristics-service');

class SmartphoneCharacteristicsController {
  async getSmartphoneCharacteristics(req, res) {
    try {
      const characteristics = await SmartphoneCharacteristicsService.getSmartphoneCharacteristics(req.query?._id);
      res.status(200).json(characteristics);
    } catch (error) {
      res.status(400).json({ error })
    }
  }
}

module.exports = new SmartphoneCharacteristicsController();
