const ProductsService = require('../services/products-service');

class ProductsController {
  async getProducts(req, res) {
    try {
      const products = await ProductsService.getProducts();
      res.status(200).json(products);
    } catch (error) {
      res.status(400).json({ error })
    }
  }
}

module.exports = new ProductsController();
