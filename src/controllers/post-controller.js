const PostService = require('../services/post-service');

class PostController {
  async setPost(req, res) {
    try {
      const post = await PostService.setPost(req.body);
      res.status(201).json(post);
    } catch (e) {
      res.status(400).json({ error: e.message || e });
    }
  }

  async getPosts(req, res) {
    try {
      const posts = await PostService.getPosts();
      res.status(200).json(posts);
    } catch (e) {
      res.status(400).json({ error: e.message || e });
    }
  }

  async getPost(req, res) {
    try {
      const post = await PostService.getPost(req.params);
      res.json(post);
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  async editPost(req, res) {
    try {
      const updatePost = await PostService.editPost(req.body);
      res.json(updatePost);
    } catch (error) {
      res.status(400).json(error)
    }
  }

  async removePost(req, res) {
    try {
      const post = await PostService.removePost(req.params);
      res.status(201).json({data: `Пост с номером Id ${post._id} успешно удален`});
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  async removeAllPosts(req, res) {
    try {
      await PostService.removeAllPosts();
      res.status(201).json({data: 'Все посты удалены'});
    } catch (error) {
      res.status(400).json({ error })
    }
  }
}

module.exports = new PostController();
