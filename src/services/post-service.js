const Post = require("../models/post-model");

class PostService {
  async setPost(post) {
    const createdPost = await Post.create(post);
    return createdPost;
  }

  async getPosts() {
    const posts = await Post.find({});
    return posts;
  }

  async getPost(id) {
    if (!id) throw new Error('Id не указан');
    const post = await Post.findById(id);
    return post;
  }

  async editPost(post) {
    if (!post._id) throw new Error('Id не указан');
    const updatePost = await Post.findByIdAndUpdate(post._id, post, { new: true });
    return updatePost;
  }

  async removePost({ id }) {
    if (!id) throw new Error('Id не был указан' );
    const post = await Post.findByIdAndDelete(id);
    return post;
  }

  async removeAllPosts() {
    const posts = await Post.deleteMany({});
    return posts;
  }
}

module.exports = new PostService();
