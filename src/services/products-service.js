const Products = require("../models/products-model");

class ProductsService {
  async getProducts() {
    const products = await Products.find({});
    return products;
  }
}

module.exports = new ProductsService();
