const Filters = require("../models/filters-model");

class FiltersService {
  async getFilters() {
    const filters = await Filters.find({_id: 'smartphones'});
    return filters;
  }
}

module.exports = new FiltersService();
