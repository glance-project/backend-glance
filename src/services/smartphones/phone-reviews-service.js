const PhoneReviewsSchema = require("../../models/smartphones/phone-reviews-model");
const { model } = require('mongoose');

class PhoneReviewsService {
  async getReviews(collection) {
    const PhoneReviews = model(`${collection}-reviews`, PhoneReviewsSchema);

    const phone_reviews = await PhoneReviews.find({});
    return phone_reviews;
  }
}

module.exports = new PhoneReviewsService();
