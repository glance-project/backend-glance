const Smartphones = require("../../models/smartphones/smartphones-model");

class SmartphonesService {
  async getSmartphones(query) {
    const {
      // page,
      // limit,
      brands
    } = query;

    // const limitPage = limit || 8;
    // const skip = limitPage * ((page || 1) - 1); // todo подумать о лимитах при сортировках

    let params = {};
    if (brands) params.brand = { "$in": brands.split(',') };
    const smartphones = await Smartphones.find(params) //.skip(skip).limit(limitPage);
    return smartphones;
  }

  async getSmartphone(_id) {
    const smartphone = await Smartphones.findOne({_id}) //.skip(skip).limit(limitPage);
    return smartphone;
  }
}

module.exports = new SmartphonesService();
