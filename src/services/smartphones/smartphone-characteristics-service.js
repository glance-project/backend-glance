const SmartphoneCharacteristics = require("../../models/smartphones/smartphone-characteristics-model");

class SmartphoneCharacteristicsService {
  async getSmartphoneCharacteristics(model) {
    const characteristics = await SmartphoneCharacteristics.findOne({_id: model});
    return characteristics;
  }
}

module.exports = new SmartphoneCharacteristicsService();
